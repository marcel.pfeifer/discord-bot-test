const axios = require('axios');
const Discord = require('discord.js');
const client = new Discord.Client();

require('dotenv').config()
 
client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});
 
client.on('message', msg => {
  if (msg.content === '!launches') {
    axios.get('https://api.spacexdata.com/v4/launches/upcoming').then(res => {
        res.data.forEach(launch => {
            const date = new Date(launch.date_unix * 1000)
            msg.reply(date.toLocaleDateString('de') + ': ' + launch.name);
        })
        
    })
  }

  if (msg.content === '!nextlaunch') {
    axios.get('https://api.spacexdata.com/v4/launches/next').then(res => {
          let launch = res.data
          const date = new Date(launch.date_unix * 1000)
          msg.reply(date.toLocaleDateString('de') + ': ' + launch.name);
          if (launch.details) msg.reply(launch.details)
    })
  }

  if (msg.content === '!roadster') {
    axios.get('https://api.spacexdata.com/v4/roadster').then(res => {
          let roadster = res.data
          msg.reply(roadster.details);
          msg.reply(roadster.flickr_images[Math.floor(Math.random() * roadster.flickr_images.length)]);
    })
  }
});

 
client.login(process.env.BOT_TOKEN);